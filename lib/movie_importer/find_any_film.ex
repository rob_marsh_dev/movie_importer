defmodule FindAnyFilm do
  @moduledoc """
  This module makes requests to the Find Any Film API.
  """
  use Tesla

  plug Tesla.Middleware.BaseUrl, "https://www.findanyfilm.com/"
  plug Tesla.Middleware.JSON

  def cinema_listings(cinema_id, date) do
    get("/api/screenings/by_venue_id/venue_id/#{cinema_id}/date_from/#{date}").body["#{cinema_id}"]["films"]
  end
end
