defmodule MovieImporter.Listing do
  @moduledoc """
  This module contains the schema for movie listings  as well as 
  functions for importing.
  """
  use Ecto.Schema
  alias MovieImporter.{Movie, Cinema, Listing, Repo}

  schema "listings" do
    belongs_to :movie, Movie
    belongs_to :cinema, Cinema
    field :time, Timex.Ecto.DateTime
    
    timestamps()
  end

  def import(datetime, cinema_id, movie_id) do
    case Listing |> Repo.get_by([cinema_id: cinema_id, movie_id: movie_id, time: datetime]) do
      nil ->
        IO.puts "Listing didn't exist. Creating...\n"
        %Listing{cinema_id: cinema_id, movie_id: movie_id, time: datetime} |> Repo.insert()
      _result ->
        IO.puts "Listing already existed. Ignoring...\n"
        :ok
    end
  end
end
