defmodule MovieImporter.Mailer do
  @moduledoc false
  use Bamboo.Mailer, otp_app: :movie_importer
end
