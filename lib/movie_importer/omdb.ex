defmodule OMDb do
  @moduledoc """
  This module makes requests to the omdb API.
  """
  use Tesla

  plug Tesla.Middleware.BaseUrl, "http://www.omdbapi.com/"
  plug Tesla.Middleware.JSON

  def movie_details(movie_name, release_year) do
    api_key = Application.get_env(:movie_importer, :omdb_api_key)
    formatted_movie_name = String.replace(movie_name, " ", "+")
    get("?t=#{formatted_movie_name}&plot=full&y=#{release_year}&apikey=#{api_key}")
  end

  def movie_details_by_imdb_id(imdb_id) do
    api_key = Application.get_env(:movie_importer, :omdb_api_key)
    get("?i=#{imdb_id}&plot=full&apikey=#{api_key}")
  end
end
