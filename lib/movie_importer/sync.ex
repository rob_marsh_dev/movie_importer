defmodule MovieImporter.Sync do
  @moduledoc """
  This module ensures the movie importer runs once every 24 hours.
  """
  use GenServer

  def start_link do
    GenServer.start_link(__MODULE__, %{})
  end

  def init(state) do
    :timer.send_interval(24 * 60 * 60 * 1000, self(), :work) # Sets listings to be improted every 24 hours
    {:ok, state}
  end

  def handle_info(:work, state) do
    MovieImporter.init(:ok)
    {:noreply, state}
  end
end
