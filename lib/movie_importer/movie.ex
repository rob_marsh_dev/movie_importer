defmodule MovieImporter.Movie do
  @moduledoc """
  This module contains the schema for movies as well as 
  functions for importing.
  """
  use Ecto.Schema
  alias MovieImporter.{Movie, MovieDetail, Listing, Repo}

  schema "movies" do
    field :name, :string
    field :release_year, :integer
    field :external_id, :integer
    has_one :movie_details, MovieDetail
    has_many :listings, Listing

    timestamps()
  end

  def import(%{"film_id" => external_id, "film_title" => name, "release_year" => release_year}) do
    case Movie |> Repo.get_by(external_id: String.to_integer(external_id)) do
      nil ->
        {:ok, _movie} =
          %Movie{name: name, release_year: String.to_integer(release_year), external_id: String.to_integer(external_id)}
          |> Repo.insert()
      result ->
        #film already exists - don't add it
        {:ok, result}
    end
  end
end
