defmodule MovieImporter.MovieDetail do
  @moduledoc """
  This module contains the schema for movie metadata as well as 
  functions for parsing and importing.
  """
  use Ecto.Schema
  import Ecto.Changeset
  alias MovieImporter.{Email, Mailer, Repo, MovieDetail}

  schema "movie_details" do
    belongs_to :movie, Movie

    field :rated, :string
    field :released, Timex.Ecto.DateTime
    field :run_time, :string
    field :plot, :string
    field :poster, :string
    field :metascore, :integer
    field :imdb_rating, :float

    timestamps()
  end

  def changeset(movie_details, params \\ %{}) do
    movie_details
    |> cast(params, [:movie_id, :rated, :released, :run_time, :plot, :poster, :metascore, :imdb_rating])
  end

  def import(movie) do
    case MovieDetail |> Repo.get_by(movie_id: movie.id) do
      nil ->
        response = OMDb.movie_details(movie.name, movie.release_year)
        details = response.body

        case details["Response"] do
          "True" ->
            movie_details = MovieDetail.parse(movie.id, details)
            case Repo.insert(movie_details) do
              {:ok, movie_detail} ->
                movie_detail
              {:error, _} ->
                []
            end
          _ ->
            Repo.insert(%MovieDetail{movie_id: movie.id})
            movie |> Email.missing_movie_details |> Mailer.deliver_later
        end
      _result ->
        :ok
    end
  end

  def parse(movie_id, details) do
    metascore =
      case details["Metascore"] do
        "N/A" ->
          nil
        _value ->
          String.to_integer(details["Metascore"])
      end
    imdb_rating =
      case details["imdbRating"] do
        "N/A" ->
          nil
        _value ->
          String.to_float(details["imdbRating"])
      end

    %MovieDetail{movie_id: movie_id, rated: details["Rated"],
    released: Timex.parse!(details["Released"], "%d %b %Y", :strftime), run_time: details["Runtime"],
    plot: details["Plot"], poster: details["Poster"],
    metascore: metascore, imdb_rating: imdb_rating}
  end
end
