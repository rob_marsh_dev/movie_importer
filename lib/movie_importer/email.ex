defmodule MovieImporter.Email do
  @moduledoc """
  This module specifies emails that are sent by the system.
  """
  import Bamboo.Email

  def missing_movie_details(movie) do
    new_email()
    |> to("rob.dmind@gmail.com")
    |> from("notifications@hullcinemaclub.com")
    |> subject("Missing Movie Information for #{movie.name}")
    |> text_body("Missing movie information for #{movie.name} with id #{movie.id}. Please rectify ASAP.")
  end
end
