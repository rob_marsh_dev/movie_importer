defmodule MovieImporter.Cinema do
  @moduledoc """
  This module provides the schema for cinemas.
  """
  use Ecto.Schema
  alias MovieImporter.Listing

  schema "cinemas" do
    field :external_id, :integer
    field :name, :string
    has_many :listings, Listing

    timestamps()
  end
end
