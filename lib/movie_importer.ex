defmodule MovieImporter do
  @moduledoc """
  This module is the entry point for the application and calls
  a number of different import functions for different entities in
  the system.
  """
  use Application
  use Timex
  alias MovieImporter.{Cinema, Movie, MovieDetail, Listing, Repo}

  # See http://elixir-lang.org/docs/stable/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    children = [
      # Start the Ecto repository
      supervisor(MovieImporter.Repo, []),
      worker(MovieImporter.Sync, [])
      # Here you could define other workers and supervisors as children
      # worker(MovieImporter.Worker, [arg1, arg2, arg3]),
    ]

    # See http://elixir-lang.org/docs/stable/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: MovieImporter.Supervisor]
    Supervisor.start_link(children, opts)
  end

  def init(:ok) do
    Cinema |>
    Repo.all |>
    Enum.each(fn (cinema) ->
      Enum.each(0..4, fn (day) ->
        {:ok, date} = 
          Timex.today |> 
          Timex.shift(days: day) |> 
          Timex.format("%Y-%m-%d", :strftime)
        movies = FindAnyFilm.cinema_listings(cinema.external_id, date)
        Enum.each(movies, fn({_movie_id, movie_data}) ->
          {:ok, movie} = Movie.import(movie_data["film_data"])
          MovieDetail.import(movie)

          Enum.each(movie_data["showings"], fn (showing) ->
            {:ok, datetime} = Timex.parse(showing["showtime"], "{ISO:Extended}")
            Listing.import(datetime, cinema.id, movie.id)
          end)
        end)
        IO.puts "Import for cinema #{cinema.id} on date #{date} import complete."
      end)
    end)
  end

  def update(id, imdb_id) do
    existing_movie_details = MovieDetail |> Repo.get_by(movie_id: id)
    response = OMDb.movie_details_by_imdb_id(imdb_id)
    details = response.body

    case details["Response"] do
      "True" -> 
        movie_details = MovieDetail.parse(existing_movie_details.movie_id, details)
        changeset = MovieDetail.changeset(existing_movie_details,
          %{movie_id: movie_details.movie_id,
          rated: movie_details.rated,
          released: movie_details.released,
          run_time: movie_details.run_time,
          plot: movie_details.plot,
          poster: movie_details.poster,
          metascore: movie_details.metascore,
          imdb_rating: movie_details.imdb_rating})

        case Repo.update(changeset) do
          {:ok, _} ->
            IO.puts "Successfully updated movie!"
          {:error, _} ->
            IO.puts "An error occured when adding the movie"
        end
      _ ->
        :noop
    end
  end
end
