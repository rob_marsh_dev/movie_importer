import Ecto.Query, only: [from: 2]

MovieImporter.Repo.insert!(%MovieImporter.Cinema{external_id: 9669, name: "VUE Hull Princes Quay"})
MovieImporter.Repo.insert!(%MovieImporter.Cinema{external_id: 9426, name: "Odeon Hull"})
MovieImporter.Repo.insert!(%MovieImporter.Cinema{external_id: 10647, name: "The Reel Cinema Hull"})
MovieImporter.Repo.insert!(%MovieImporter.Cinema{external_id: 7523, name: "Cineworld Hull"})
