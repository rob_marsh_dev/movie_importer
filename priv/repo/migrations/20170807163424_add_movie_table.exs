defmodule MovieImporter.Repo.Migrations.AddMovieTable do
  use Ecto.Migration

  def change do
    create table(:movie_details) do
      add :movie_id, :integer
      add :rated, :string
      add :released, :datetime
      add :run_time, :string
      add :plot, :string
      add :poster, :string
      add :metascore, :integer
      add :imdb_rating, :float

      timestamps()
    end

    create table(:movies) do
      add :name, :string
      add :movie_details_id, :integer

      timestamps()
    end

    alter table(:listings) do
      remove :name
      add :movie_id, :integer
    end
  end
end
