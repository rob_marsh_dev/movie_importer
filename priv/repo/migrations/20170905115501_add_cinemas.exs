defmodule MovieImporter.Repo.Migrations.AddCinemas do
  use Ecto.Migration

  def change do
    create table(:cinemas) do
      add :extenal_id, :integer
      add :name, :string

      timestamps()
    end

    alter table(:listings) do
      add :cinema_id, references(:cinemas)
    end
  end
end
