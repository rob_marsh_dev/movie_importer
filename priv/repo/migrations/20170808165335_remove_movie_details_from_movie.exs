defmodule MovieImporter.Repo.Migrations.RemoveMovieDetailsFromMovie do
  use Ecto.Migration

  def change do
    alter table(:movies) do
      remove :movie_details_id
    end
  end
end
