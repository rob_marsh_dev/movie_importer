defmodule MovieImporter.Repo.Migrations.ExtendMoviePlotLength do
  use Ecto.Migration

  def change do
    alter table(:movie_details) do
      modify :plot, :text
    end
  end
end
