defmodule MovieImporter.Repo.Migrations.AddListings do
  use Ecto.Migration

  def change do
    create table(:listings) do
      add :name, :string
      add :time, :date

      timestamps()
    end
  end
end
