defmodule MovieImporter.Repo.Migrations.DropAndRecreateListings do
  use Ecto.Migration

  def change do
    drop table(:listings)
    
    create table(:listings) do
      add :name, :string
      add :time, :datetime

      timestamps()
    end
  end
end
