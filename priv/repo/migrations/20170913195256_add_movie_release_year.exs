defmodule MovieImporter.Repo.Migrations.AddMovieReleaseYear do
  use Ecto.Migration

  def change do
    alter table(:movies) do
      add :release_year, :integer
      add :external_id, :integer
    end
  end
end
