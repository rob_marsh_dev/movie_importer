defmodule MovieImporter.Repo.Migrations.AddMovieRelationships do
  use Ecto.Migration

  def change do
    alter table(:movie_details) do
      modify :movie_id, references(:movies)
    end

    alter table(:listings) do
      modify :movie_id, references(:movies)
    end
  end
end
