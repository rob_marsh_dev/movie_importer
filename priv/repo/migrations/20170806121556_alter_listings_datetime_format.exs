defmodule MovieImporter.Repo.Migrations.AlterListingsDatetimeFormat do
  use Ecto.Migration

  def change do
    alter table(:listings) do
      modify :time, :datetime
      modify :inserted_at, :datetime
      modify :updated_at, :datetime
    end

    # drop table(:listings)

    # create table(:listings) do
    #   add :name, :string
    #   add :time, :datetime

    #   timestamps()
    # end
  end
end
