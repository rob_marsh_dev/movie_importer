defmodule MovieImporter.Repo.Migrations.FixCinemaSpelling do
  use Ecto.Migration

  def change do
    alter table(:cinemas) do
      remove :extenal_id
      add :external_id, :integer
    end
  end
end
