defmodule MovieImporter.Mixfile do
  use Mix.Project

  def project do
    [app: :movie_importer,
     version: "0.1.0",
     elixir: "~> 1.4",
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     aliases: aliases(),
     deps: deps()]
  end

  # Configuration for the OTP application
  #
  # Type "mix help compile.app" for more information
  def application do
    # Specify extra applications you'll use from Erlang/Elixir
    [extra_applications: [:logger, :inets, :ecto, :postgrex, :timex, :timex_ecto, :bamboo, :bamboo_smtp, :edeliver],
    mod: {MovieImporter, []}]
  end

  # Dependencies can be Hex packages:
  #
  #   {:my_dep, "~> 0.3.0"}
  #
  # Or git/path repositories:
  #
  #   {:my_dep, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}
  #
  # Type "mix help deps" for more examples and options
  defp deps do
    [{:tesla, "~> 0.7.1"},
     {:poison, ">= 1.0.0"},
     {:postgrex, ">= 0.0.0"},
     {:ecto, "~> 2.1"},
     {:timex, "~> 3.1"},
     {:timex_ecto, "~> 3.1"},
     {:bamboo, "~> 0.8"},
     {:bamboo_smtp, "~> 1.4.0"},
     {:edeliver, "~> 1.4.4"},
     {:credo, "~> 0.8", only: [:dev, :test], runtime: false},
     {:distillery, ">= 0.8.0", warn_missing: false}]
  end

  # Aliases are shortcut or tasks specific to the current project.
  # For example, to create, migrate and run the seeds file at once:
  #
  #     $ mix ecto.setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    ["ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
     "ecto.reset": ["ecto.drop", "ecto.setup", "run priv/repo/seeds.exs"]]
  end
end
