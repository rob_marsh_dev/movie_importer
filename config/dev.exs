use Mix.Config

if File.exists?("config/dev.secret.exs") do
  import_config "dev.secret.exs"
end

# config :movie_importer, MovieImporter.Mailer,
#   adapter: Bamboo.LocalAdapter